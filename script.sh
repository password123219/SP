#!/bin/bash


echo "Автор: Безруков Павел Павлович 729-1"
echo "Название программы: Установка пакетов"
echo "Описание: Скрипт, позволяющий пользователю установить указанный пакет или найти его в репозиториях"

# Бесконечный цикл
while :
do
    # Запрос имени пакета
    read -p "Введите имя пакета: " package_name

    # Проверка установлен ли пакет
    if rpm -q "$package_name" > /dev/null; then
        # Пакет установлен, вывод информации о нём
        rpm -qi "$package_name"
    else
        # Пакет не установлен, ищем его в репозиториях
        if yum search "$package_name" | grep -q "^$package_name\."; then
            # Пакет найден, предлагаем установить
            read -p "Пакет $package_name найден в репозиториях. Хотите установить его? (y/n): " install_choice
            if [ "$install_choice" == "y" ]; then
                # Устанавливаем пакет
                yum install "$package_name"
            fi
        else
            # Пакет не найден
            echo "Пакет $package_name не найден в репозиториях."
        fi
    fi

    # Предложение начать выполнение сначала или выйти из цикла
    read -p "Хотите начать сначала? (y/n): " continue_choice
    if [ "$continue_choice" != "y" ]; then
        # Выход из цикла
        break
    fi
done

exit 0
